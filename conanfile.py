import os
from os import path

from conans import ConanFile, tools
from conans.tools import download, unzip, untargz, check_sha256, pythonpath

class Recipe(ConanFile):
    name        = "libsigcplusplus"
    libname     = "libsigc++"
    version     = "2.10.0"
    description = "libsigc++ implements a typesafe callback system for standard C++"
    license     = "LGPL"
    url         = "https://gitlab.com/no-face/libsigcplusplus-conan"  #recipe repo url
    lib_urls   = {
        "project" : "http://libsigc.sourceforge.net",
        "repo"    : "https://git.gnome.org/browse/libsigcplusplus",
        "mirror"  : "https://github.com/GNOME/libsigcplusplus",
        "docs"    : "http://libsigc.sourceforge.net/doc.shtml"
    }

    settings    = "os", "compiler", "arch"
    options = {
        "shared" : [True, False],
        "pcre"   : ["default", "internal", "system"]
    }
    default_options = (
        "shared=True",
        "pcre=internal"
    )

    build_requires    = (
        "untarxz/0.0.1@noface/testing",
        "AutotoolsHelper/0.0.1@noface/experimental"
    )

    BASE_URL_DOWNLOAD       = "https://download.gnome.org/sources/libsigc++"
    FILE_URL                = "{}/2.10/{}-{}.tar.xz".format(BASE_URL_DOWNLOAD, libname, version)
    EXTRACTED_FOLDER_NAME   = "{}-{}".format(libname, version)
    FILE_SHA256             = 'f843d6346260bfcb4426259e314512b99e296e8ca241d771d21ac64f28298d81'

    def source(self):
        zip_name = self.name + ".tar.xz"
        download(self.FILE_URL, zip_name)
        check_sha256(zip_name, self.FILE_SHA256)

        with pythonpath(self):
            from untarxz import untarxz
            untarxz(self, zip_name)

    def build(self):
        self.prepare_build()
        self.configure_and_make()

    def package(self):
        #Files installed in build step
        pass

    def package_info(self):
        libdir = "sigc++-2.0"

        includes = [
            path.join("include", libdir),
            path.join("lib", libdir, "include") # adds config.h
        ]

        self.cpp_info.includedirs   = includes
        self.cpp_info.resdirs       = ['share']
        self.cpp_info.libs          = ["sigc-2.0"]
    
    ##################################################################################################
    
    def prepare_build(self):
        self.output.info("preparing build")

        if getattr(self, "package_dir", None) is None:
            #Make install dir
            self.package_dir = path.abspath(path.join(".", "install"))
            self._try_make_dir(self.package_dir)
    
    def configure_and_make(self):
        with tools.chdir(self.EXTRACTED_FOLDER_NAME), pythonpath(self):
            from autotools_helper import Autotools

            autot = Autotools(self,
                shared      = self.options.shared)

            # For now, disable boost
            autot.with_feature("boost", "no")

            autot.configure()
            autot.build()
            autot.install()

    def _try_make_dir(self, dir):
        try:
            os.mkdir(dir)
        except OSError:
            #dir already exist
            pass
