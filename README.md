# libsigc++-conan

[Conan.io](https://www.conan.io/) package for [libsigc++](http://libsigc.sourceforge.net/) library.

## How to use

See [conan docs](http://docs.conan.io/en/latest/) for instructions in how to use conan, and
libsigc++ [documentation](http://libsigc.sourceforge.net/doc.shtml) to instruction in how to use the library.


## About libglib

    libsigc++ implements a typesafe callback system for standard C++.
    It allows you to define signals and to connect those signals to any callback function,
    either global or a member function, regardless of whether it is static or virtual.

    It also contains adaptor classes for connection of dissimilar callbacks and has an ease of
    use unmatched by other C++ callback libraries.

Reference: [libsigc++ about](http://libsigc.sourceforge.net/).


## License

This conan package is distributed under the [unlicense](http://unlicense.org/) terms (see LICENSE.md).

Libsigc++ is licensed under the GNU Library General Public License, [LGPL](http://www.gnu.org/copyleft/lgpl.html).
